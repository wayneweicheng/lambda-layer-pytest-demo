Use sam for local testing

sam init
# this downloads the layer if the layer ARN is specified in CFN.
sam local invoke "DeserializeDemoFunction" --no-event

sam local start-api --port 3010