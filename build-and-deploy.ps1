
#sam build
sam build

# Package the solution
sam package --template-file template.yaml --output-template-file deploy.yaml --s3-bucket wayne-serverless-deploy

# Deploy the artifact to aws
sam deploy --template-file .\deploy.yaml --stack-name LayersDemoStack2
